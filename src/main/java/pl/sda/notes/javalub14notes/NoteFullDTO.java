package pl.sda.notes.javalub14notes;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class NoteFullDTO extends NoteDTO {

    private LocalDateTime createDate;
    private LocalDateTime updateDate;

    public NoteFullDTO(Long id, String title, String text, LocalDateTime createDate, LocalDateTime updateDate) {
        super(id, title, text);
        this.createDate = createDate;
        this.updateDate = updateDate;
    }
}
