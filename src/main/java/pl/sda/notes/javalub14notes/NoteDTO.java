package pl.sda.notes.javalub14notes;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NoteDTO extends NoteMinDTO {
    private String text;

    public NoteDTO(Long id, String title, String text) {
        super(id, title);
        this.text = text;
    }
}
