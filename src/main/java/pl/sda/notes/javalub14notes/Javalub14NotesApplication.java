package pl.sda.notes.javalub14notes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Javalub14NotesApplication {

    public static void main(String[] args) {
        SpringApplication.run(Javalub14NotesApplication.class, args);
    }

}
