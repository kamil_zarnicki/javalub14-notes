package pl.sda.notes.javalub14notes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Transactional
@Service
public class NotesService {

    private final NotesRepository notesRepository;

    public void create(NoteDTO dto) {
        log.info("create -> dto: {}", dto);
        Notes saved = notesRepository.save(new Notes(dto.getTitle(), dto.getText()));
        log.info("New note created with id {}", saved.getId());
    }

    public void update(NoteDTO dto) {
        log.info("update -> dto: {}", dto);
        Notes note = notesRepository.findByIdAndDeletedIsFalse(dto.getId())
                .orElseThrow(() -> new NoteDoesNotExistException(dto.getId()));
        note.setTitle(dto.getTitle());
        note.setText(dto.getText());
        notesRepository.save(note);
        log.info("Note with id {} updated.", dto.getId());
    }

    public void delete(Long id) {
        log.info("delete -> id: {}", id);
        notesRepository.delete(id);
        log.info("Note with id {} marked as deleted.", id);
    }

    public List<NoteMinDTO> list() {
        log.info("list");
        return notesRepository.findAllByDeletedIsFalse().stream()
                .map(n -> new NoteMinDTO(n.getId(), n.getTitle()))
                .collect(Collectors.toList());
    }

    public NoteFullDTO getOne(Long id) {
        log.info("getOne for id: {}", id);
        return notesRepository.findByIdAndDeletedIsFalse(id)
                .map(e -> new NoteFullDTO(e.getId(),
                        e.getTitle(),
                        e.getText(),
                        e.getCreateDate(),
                        e.getUpdateDate()))
                .orElseThrow(() -> new NoteDoesNotExistException(id));
    }

    public List<NoteMinDTO> search(String text) {
        log.info("Search for notes containing '{}'", text);
        return notesRepository.search(text).stream()
                .map(notes -> new NoteMinDTO(notes.getId(), notes.getTitle()))
                .collect(Collectors.toList());
    }
}
