package pl.sda.notes.javalub14notes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NotesRepository extends JpaRepository<Notes, Long> {

    Optional<Notes> findByIdAndDeletedIsFalse(Long id);

    @Modifying
    @Query("update Notes n set n.deleted = true where n.id = ?1")
    void delete(Long id);

    List<Notes> findAllByDeletedIsFalse();

    @Query("select n from Notes n where n.deleted = false and (n.title like %?1% or n.text like %?1%)")
    List<Notes> search(String text);
}
