package pl.sda.notes.javalub14notes;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoteDoesNotExistException extends RuntimeException {

    public NoteDoesNotExistException(Long id) {
        super(String.format("Note with id %d does not exist.", id));
    }
}
