package pl.sda.notes.javalub14notes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoteMinDTO {

    private Long id;
    private String title;
}
