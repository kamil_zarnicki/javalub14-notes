package pl.sda.notes.javalub14notes;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/notes")
public class NotesController {

    private final NotesService notesService;

    @PutMapping
    public void create(@RequestBody NoteDTO dto) {
        log.info("PUT /note with payload: {}", dto);
        notesService.create(dto);
    }

    @PatchMapping
    public void update(@RequestBody NoteDTO dto) {
        log.info("PATCH /note with payload: {}", dto);
        notesService.update(dto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        log.info("DELETE /note/{}", id);
        notesService.delete(id);
    }

    @GetMapping
    public List<NoteMinDTO> list() {
        log.info("GET /notes");
        return notesService.list();
    }

    @GetMapping("/{id}")
    public NoteFullDTO getOne(@PathVariable("id") Long id) {
        log.info("GET /note/{}", id);
        return notesService.getOne(id);
    }

    @GetMapping("/search")
    public List<NoteMinDTO> search(@PathParam("text") String text) {
        log.info("GET /search -> text: {}", text);
        return notesService.search(text);
    }
}
